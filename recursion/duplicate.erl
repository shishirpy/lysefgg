-module(duplicate).
-export([duplicate/2, tail_duplicate/2]).

duplicate(0, _) -> [];
duplicate(N, Term) when N > 0 -> [Term|duplicate(N-1, Term)].


tail_duplicate(N, Term) -> tail_duplicate(N, Term, []).

tail_duplicate(0, _, Acc) -> Acc;
tail_duplicate(N, Term, Acc) -> tail_duplicate(N-1, Term, [Term|Acc]).