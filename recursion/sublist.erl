-module(sublist).
-export([sublist/2, tail_sublist/2]).

sublist(_, 0) -> [];
sublist([], _) -> [];
sublist([H|T], N) when N > 0 -> [H| sublist(T, N-1)].


tail_sublist(List, N) -> tail_sublist(List, N, []).

tail_sublist(_, 0, Acc) -> Acc;
tail_sublist([], _, Acc) -> Acc;
tail_sublist([H|T], N, Acc) when N > 0 -> tail_sublist(T, N-1, Acc ++ [H]).